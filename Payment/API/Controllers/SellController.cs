using Microsoft.AspNetCore.Mvc;
using Payment.Domain.Repositories;
using Payment.API.DTO;


namespace Payment.API.Controllers;

[ApiController]
[Route("[controller]")]
public class SellController : ControllerBase
{
    private readonly ISellRepository _repository;
    public SellController(ISellRepository repository)
    {
        _repository = repository;
    }

    /// <summary>
    /// Registra uma venda
    /// </summary>
    /// <param name="dto">Payload</param>
    /// <remarks>
    /// Endpoint para o registro de uma venda. Espera-se que sejam informados os dados do vendedor e os dados dos itens que serão vendidos. Deve haver ao menos um item a ser vendido.
    /// </remarks>
    /// <response code="201">Venda registrada com sucesso</response>
    /// <response code="404">Ocorreu um erro ao registrar a venda</response>
    [HttpPost()]
    public IActionResult Registrar([FromBody] SellRequestDTO dto)
    {
        try
        {
            dto.Validate();

            var response = _repository.RegisterSell(dto);
            return CreatedAtAction(nameof(Buscar), new { id = response.Id }, response);
        }
        catch (Exception ex)
        {
            return NotFound(ex.Message);
        }
    }


    /// <summary>
    /// Busca os dados de uma venda
    /// </summary>
    /// <param name="id">Número do ID da venda que se deseja buscar</param>
    /// <remarks>
    /// Endpoint para buscar os dados de uma venda a partir do seu ID. 
    /// </remarks>
    /// <response code="200">Busca realizada com sucesso</response>
    /// <response code="404">O ID da venda informada não foi encontrado ou ocorreu um erro</response>
    [HttpGet("{id}")]
    public IActionResult Buscar(int id)
    {
        try
        {
            var response = _repository.GetSellById(id);
            return Ok(response);
        }
        catch (Exception ex)
        {
            return NotFound(ex.Message);
        }
    }


    /// <summary>
    /// Atualiza o status de uma venda
    /// </summary>
    /// <param name="id">Número do ID da venda que se deseja buscar</param>
    /// <param name="dto">Payload</param>
    /// <remarks>
    /// Endpoint para atualizar o status de uma venda a partir do seu ID. 
    /// No payload, espera-se que seja informado o novo status da venda. 
    /// 
    /// Os valores aceitos são: Pagamento_Aprovado, Enviado_Para_Transportadora, Entregue, Cancelada
    /// 
    /// O novo status será validado a partir do status atual da venda.
    /// </remarks>
    /// <response code="204">Atualização realizada com sucesso</response>
    /// <response code="404">O ID da venda informada não foi encontrado ou ocorreu um erro</response>
    [HttpPatch("{id}")]
    public IActionResult AtualizarVenda(int id, [FromBody] UpdateStatusRequestDTO dto)
    {
        try
        {
            dto.Validate();

            _repository.UpdateSellStatus(id, dto.GetStatus());
            return NoContent();
        }
        catch (Exception ex)
        {
            return NotFound(ex.Message);
        }

    }
}