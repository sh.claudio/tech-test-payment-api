using System.ComponentModel.DataAnnotations;
using Payment.Domain.Entities;
using Payment.Shared;

namespace Payment.API.DTO;

public class SellerDTO
{
    /// <summary>
    /// CPF do vendedor
    /// </summary>
    /// <example>00000000191</example>
    [Required]
    public string? Cpf { get; set; }
    /// <summary>
    /// Nome do vendedor
    /// </summary>
    /// <example>João Dias</example>
    [Required]
    public string? Nome { get; set; }
    /// <summary>
    /// E-Mail do vendedor
    /// </summary>
    /// <example>joao.dias@teste.com</example>
    [Required]
    public string? EMail { get; set; }
    /// <summary>
    /// Telefone do vendedor
    /// </summary>
    /// <example>(11) 99999-9999</example>
    [Required]
    public string? Telefone { get; set; }

    public Seller ToEntity()
    {
        var entity = new Seller();

        entity.Cpf = Cpf;
        entity.Name = Nome;
        entity.EMail = EMail;
        entity.Phone = Telefone;

        return entity;
    }

    public void FromEntity(Seller entity)
    {
        Cpf = entity.Cpf;
        Nome = entity.Name;
        EMail = entity.EMail;
        Telefone = entity.Phone;
    }

    public void Validate()
    {
        if (!CustomValidations.CheckCPF(cpf: Cpf))
            throw new ArgumentException("O CPF do vendedor informado é inválido");

        if (string.IsNullOrEmpty(Nome))
            throw new ArgumentException("O nome do vendedor não foi informado");

        if (!CustomValidations.CheckEMail(email: EMail))
            throw new ArgumentException("O E-Mail do vendedor informado é inválido");

        if (string.IsNullOrEmpty(Telefone))
            throw new ArgumentException("O telefone do vendedor não foi informado");
    }


}