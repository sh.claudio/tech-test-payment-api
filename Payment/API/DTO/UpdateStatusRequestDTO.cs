using System.ComponentModel.DataAnnotations;
using Payment.Domain.Enums;

namespace Payment.API.DTO;

public class UpdateStatusRequestDTO
{
    /// <summary>
    /// Novo status da venda a ser atualizada. Valores aceitos: Pagamento_Aprovado, Enviado_Para_Transportadora, Entregue, Cancelada
    /// </summary>
    /// <example>Pagamento_Aprovado</example>
    [Required]
    public string? NewStatus { get; set; }

    public ESellStatus GetStatus()
    {
        ESellStatus retorno;
        Enum.TryParse<ESellStatus>(NewStatus, out retorno);

        return retorno;
    }

    public void Validate()
    {
        if (string.IsNullOrEmpty(NewStatus))
            throw new ArgumentException($"O status não foi informado");

        if (!Enum.TryParse(typeof(ESellStatus), NewStatus, true, out _))
            throw new ArgumentException($"O status {NewStatus} não consta na lista de valores aceitos");
    }
}