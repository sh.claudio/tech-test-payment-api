using Payment.Domain.Entities;
using Payment.Domain.Enums;

namespace Payment.API.DTO;

public class SellResponseDTO
{
    public int Id { get; set; }
    public SellerDTO? Vendedor { get; set; }
    public DateTime Data { get; set; }
    public ESellStatus Status { get; set; }
    public ICollection<SellItemDTO>? ItensVendidos { get; set; }

    public SellResponseDTO()
    {
        Vendedor = new SellerDTO();
        Data = new DateTime();
        ItensVendidos = new List<SellItemDTO>();
    }

    public void FromEntity(Sell entity)
    {
        Id = entity.Id;
        Vendedor?.FromEntity(entity.Seller!);
        Data = entity.Date;
        Status = entity.Status;
        ItensVendidos = entity.SellItems?.Select(item =>
        {
            var dto = new SellItemDTO();
            dto.FromEntity(item);
            return dto;
        }).ToList();
    }


}