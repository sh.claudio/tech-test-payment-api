using System.ComponentModel.DataAnnotations;
using Payment.Domain.Entities;

namespace Payment.API.DTO;

public class ProductDTO
{
    /// <summary>
    /// Código do produto
    /// </summary>
    /// <example>789325551166</example>
    [Required]
    public string? Codigo { get; set; }
    /// <summary>
    /// Descrição do produto
    /// </summary>
    /// <example>TV OLED 75 pol</example>
    [Required]
    public string? Descricao { get; set; }
    /// <summary>
    /// Preço do produto
    /// </summary>
    /// <example>8200</example>
    [Required]
    public decimal Preco { get; set; }

    public Product ToEntity()
    {
        var entity = new Product();

        entity.Code = Codigo;
        entity.Description = Descricao;
        entity.Price = Preco;

        return entity;
    }

    public void FromEntity(Product entity)
    {
        Codigo = entity.Code;
        Descricao = entity.Description;
        Preco = entity.Price;
    }

    public void Validate()
    {
        if (string.IsNullOrEmpty(Codigo))
            throw new ArgumentException("Código do produto não preenchido");

        if (string.IsNullOrEmpty(Descricao))
            throw new ArgumentException("Descrição do produto não preenchida");

        if (Preco <= 0)
            throw new ArgumentException("O preço informado deve ser maior que zero");
    }
}