using System.ComponentModel.DataAnnotations;

namespace Payment.API.DTO;

public class SellRequestDTO
{
    /// <summary>
    /// Vendedor responsável pela venda
    /// </summary>
    [Required]
    public SellerDTO? Vendedor { get; set; }

    /// <summary>
    /// Itens a serem vendidos
    /// </summary>
    [Required]
    public ICollection<SellItemDTO>? ItensVendidos { get; set; }

    public SellRequestDTO()
    {
        Vendedor = new SellerDTO();
        ItensVendidos = new List<SellItemDTO>();
    }

    public void Validate()
    {
        if (Vendedor == null)
            throw new ArgumentException("O vendedor não foi informado");

        Vendedor?.Validate();

        if (ItensVendidos?.Count == 0)
            throw new ArgumentException("A venda deve ter ao menos um item");

        foreach (var item in ItensVendidos!)
        {
            item.Validate();
        }
    }

}