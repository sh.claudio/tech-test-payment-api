using System.ComponentModel.DataAnnotations;
using Payment.Domain.Entities;

namespace Payment.API.DTO;

public class SellItemDTO
{
    /// <summary>
    /// Produto a ser vendido
    /// </summary>
    [Required]
    public ProductDTO? Produto { get; set; }
    /// <summary>
    /// Quantidade de produtos a serem vendidos
    /// </summary>
    /// <example>2</example>
    [Required]
    public int Quantidade { get; set; }

    public SellItemDTO()
    {
        Produto = new ProductDTO();
    }

    public void FromEntity(SellItem entity)
    {
        Produto = new ProductDTO();
        Produto.FromEntity(entity.Product!);
        Quantidade = entity.Quantity;
    }
    public void Validate()
    {
        if (Produto == null)
            throw new ArgumentException("O produto para venda não foi informado");

        Produto?.Validate();

        if (Quantidade <= 0)
            throw new ArgumentException("A quantidade informada para venda deve ser maior que zero");
    }

}