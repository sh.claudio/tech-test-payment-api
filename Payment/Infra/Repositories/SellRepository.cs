using Microsoft.EntityFrameworkCore;
using Payment.API.DTO;
using Payment.Domain.Entities;
using Payment.Domain.Enums;
using Payment.Domain.Repositories;
using Payment.Infra.Context;

namespace Payment.Infra.Repositories;

public class SellRepository : ISellRepository
{
    private readonly SellContext _context;
    public SellRepository(SellContext context)
    {
        _context = context;
    }

    public SellResponseDTO GetSellById(int id)
    {
        var sell = _context
            .Sells?
            .AsNoTracking()
            .Include(x => x.Seller)
            .Include(x => x.SellItems)
            .ThenInclude(x => x.Product)
            .FirstOrDefault(x => x.Id == id);

        if (sell == null)
            throw new KeyNotFoundException($"Nenhuma venda com o Id {id} foi encontrada!");

        var response = new SellResponseDTO();
        response.FromEntity(sell);

        return response;
    }

    public SellResponseDTO RegisterSell(SellRequestDTO dto)
    {
        var sell = GetSellFromDTO(dto);
        _context.Sells?.Add(sell);
        _context.SaveChanges();

        var response = new SellResponseDTO();
        response.FromEntity(sell);

        return response;
    }

    private Sell GetSellFromDTO(SellRequestDTO dto)
    {
        var sell = new Sell();

        var seller = _context.Sellers?.FirstOrDefault(x => x.Cpf == dto.Vendedor!.Cpf);
        if (seller == null)
            sell.Seller = dto.Vendedor?.ToEntity();
        else
            sell.Seller = seller;

        sell.Date = DateTime.Now;
        sell.Status = ESellStatus.Aguardando_Pagamento;
        sell.SellItems = dto.ItensVendidos?.Select(item =>
        {
            var sellItem = new SellItem();

            var product = _context.Products?.FirstOrDefault(x => x.Code == item.Produto!.Codigo);
            if (product == null)
                sellItem.Product = item.Produto?.ToEntity();
            else
                sellItem.Product = product;

            sellItem.Quantity = item.Quantidade;

            return sellItem;
        }).ToList();

        return sell;
    }

    public void UpdateSellStatus(int id, ESellStatus newStatus)
    {
        var sell = _context.Sells?.FirstOrDefault(x => x.Id == id);

        if (sell == null)
            throw new KeyNotFoundException($"Nenhuma venda com o Id {id} foi encontrada!");

        if (!sell.ChangeStatus(newStatus))
            throw new InvalidOperationException($"A transição do status {sell.Status} para o status {newStatus} não é permitida");

        _context.SaveChanges();
    }
}