using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Infra.Context;

public class SellContext : DbContext
{
    public SellContext(DbContextOptions<SellContext> options) : base(options)
    {
    }

    public DbSet<Seller>? Sellers { get; set; }

    public DbSet<Product>? Products { get; set; }

    public DbSet<Sell>? Sells { get; set; }
}