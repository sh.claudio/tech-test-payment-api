using Payment.Domain.Enums;

namespace Payment.Shared;

public static class ESellStatusValidator
{
    private static List<ESellStatusRuleItem> Rules;

    static ESellStatusValidator()
    {
        Rules = new List<ESellStatusRuleItem>();
        LoadRules();
    }

    static void LoadRules()
    {
        var waitingPaymentRule = new ESellStatusRuleItem(ESellStatus.Aguardando_Pagamento);
        waitingPaymentRule.AddNextStatusAllowed(ESellStatus.Pagamento_Aprovado);
        waitingPaymentRule.AddNextStatusAllowed(ESellStatus.Cancelada);
        Rules.Add(waitingPaymentRule);

        var paymentApprovedRule = new ESellStatusRuleItem(ESellStatus.Pagamento_Aprovado);
        paymentApprovedRule.AddNextStatusAllowed(ESellStatus.Enviado_Para_Transportadora);
        paymentApprovedRule.AddNextStatusAllowed(ESellStatus.Cancelada);
        Rules.Add(paymentApprovedRule);

        var sentToCarrierRule = new ESellStatusRuleItem(ESellStatus.Enviado_Para_Transportadora);
        sentToCarrierRule.AddNextStatusAllowed(ESellStatus.Entregue);
        Rules.Add(sentToCarrierRule);

        var deliveredRule = new ESellStatusRuleItem(ESellStatus.Entregue);
        Rules.Add(deliveredRule);

        var cancelledRule = new ESellStatusRuleItem(ESellStatus.Cancelada);
        Rules.Add(cancelledRule);
    }

    public static bool ChangeStatusAllowed(ESellStatus currentStatus, ESellStatus newStatus)
    {
        var item = Rules.Find(ruleTo => ruleTo.Status == currentStatus);
        return item!.NextStatusesAllowed.Exists(statusAllowed => statusAllowed == newStatus);
    }

}

public class ESellStatusRuleItem
{
    public ESellStatus Status { get; set; }

    public List<ESellStatus> NextStatusesAllowed { get; set; }

    public ESellStatusRuleItem(ESellStatus status)
    {
        Status = status;
        NextStatusesAllowed = new List<ESellStatus>();
    }

    public void AddNextStatusAllowed(ESellStatus statusAllowed)
    {
        NextStatusesAllowed.Add(statusAllowed);
    }
}