using System.Net.Mail;

namespace Payment.Shared;

public static class CustomValidations
{
    public static bool CheckCPF(string cpf)
    {
        if (string.IsNullOrEmpty(cpf))
            throw new ArgumentException("O CPF não foi informado");

        cpf = OnlyNumbers(cpf);

        if (CpfLengthInvalid(cpf))
            return false;

        if (SameDigits(cpf))
            return false;

        string dv = cpf.Substring(9);

        if (!CheckFirstDV(cpf, dv))
            return false;

        if (!CheckSecondDV(cpf, dv))
            return false;

        return true;
    }

    private static string OnlyNumbers(string text)
    {
        string digits = "0123456789";
        string result = "";

        for (int i = 0; i < text.Length; i++)
        {
            if (digits.IndexOf(text.Substring(i, 1)) >= 0)
            {
                result += text.Substring(i, 1);
            }
        }
        return result;
    }

    private static bool CpfLengthInvalid(string text)
    {
        return text.Length != 11;
    }

    private static bool SameDigits(string text)
    {
        for (int i = 0; i < text.Length - 1; i++)
        {
            if (text.Substring(i, 1) != text.Substring(i + 1, 1))
            {
                return false;
            }
        }

        return true;
    }

    private static bool CheckFirstDV(string cpf, string dv)
    {
        return CheckDV(cpf.Substring(0, 9), dv.Substring(0, 1));
    }

    private static bool CheckSecondDV(string cpf, string dv)
    {
        return CheckDV(cpf.Substring(0, 10), dv.Substring(1, 1));
    }

    private static bool CheckDV(string numbers, string dv)
    {
        var len = numbers.Length + 1;
        var sum = 0;

        for (int i = len; i > 1; i--)
        {
            sum += Convert.ToInt32(numbers.Substring(len - i, 1)) * i;
        }

        var result = sum % 11 < 2 ? 0 : 11 - sum % 11;


        return result == Convert.ToInt32(dv);
    }


    public static bool CheckEMail(string email)
    {
        try
        {
            var mail = new MailAddress(email);
            return true;
        }
        catch (FormatException)
        {
            return false;
        }
    }
}