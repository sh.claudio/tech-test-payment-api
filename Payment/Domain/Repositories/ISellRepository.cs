using Payment.API.DTO;
using Payment.Domain.Enums;

namespace Payment.Domain.Repositories;

public interface ISellRepository
{
    SellResponseDTO RegisterSell(SellRequestDTO dto);

    SellResponseDTO GetSellById(int id);

    void UpdateSellStatus(int id, ESellStatus newStatus);

}