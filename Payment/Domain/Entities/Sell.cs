using System.ComponentModel.DataAnnotations.Schema;
using Payment.Domain.Enums;
using Payment.Shared;

namespace Payment.Domain.Entities;

public class Sell : Entity
{
    [ForeignKey("SellerId")]
    public int SellerId { get; set; }
    public Seller? Seller { get; set; }

    public DateTime Date { get; set; }
    public ESellStatus Status { get; set; }

    public IEnumerable<SellItem>? SellItems { get; set; }

    public bool ChangeStatus(ESellStatus newStatus)
    {
        if (!ESellStatusValidator.ChangeStatusAllowed(Status, newStatus))
            return false;

        Status = newStatus;
        return true;
    }


}