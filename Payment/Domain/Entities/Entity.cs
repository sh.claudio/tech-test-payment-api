using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Entities;

public abstract class Entity : IEquatable<Entity>
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; private set; }

    public bool Equals(Entity? other)
    {
        return Id == other?.Id;
    }
}