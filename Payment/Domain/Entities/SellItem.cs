using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Entities;

public class SellItem : Entity
{
    [ForeignKey("ProductId")]
    public int ProductId { get; set; }
    public Product? Product { get; set; }

    public int Quantity { get; set; }
}