namespace Payment.Domain.Entities;

public class Seller : Entity
{
    public string? Cpf { get; set; }
    public string? Name { get; set; }
    public string? EMail { get; set; }
    public string? Phone { get; set; }
}