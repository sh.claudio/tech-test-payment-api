using System.Text.Json.Serialization;

namespace Payment.Domain.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum ESellStatus
{
    Aguardando_Pagamento,
    Pagamento_Aprovado,
    Enviado_Para_Transportadora,
    Entregue,
    Cancelada
}