using Payment.Shared;

namespace Payment.Tests.Shared;

[TestClass]
public class CustomValidationTests
{
    [TestMethod]
    public void Check_CPF_Valid_Test()
    {
        Assert.IsTrue(CustomValidations.CheckCPF("00000000191"));
    }

    [TestMethod]
    public void Check_CPF_Invalid_Test()
    {
        Assert.IsFalse(CustomValidations.CheckCPF("00000000192"));
    }

    [TestMethod]
    public void Check_EMail_Valid_Test()
    {
        Assert.IsTrue(CustomValidations.CheckEMail("a@b.cd"));
    }

    [TestMethod]
    public void Check_EMail_Invalid_Test()
    {
        Assert.IsFalse(CustomValidations.CheckEMail("emailinvalido"));
    }

}