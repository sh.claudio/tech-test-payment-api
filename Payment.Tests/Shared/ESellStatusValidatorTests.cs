using Payment.Domain.Enums;
using Payment.Shared;

namespace Payment.Tests.Shared;

[TestClass]
public class ESellStatusValidatorTests
{
    [TestMethod]
    public void Status_Waiting_Payment_Should_Be_Allowed_To_Change_To_Cancelled()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Aguardando_Pagamento, ESellStatus.Cancelada);
        Assert.IsTrue(allowed);
    }

    [TestMethod]
    public void Status_Waiting_Payment_Should_Be_Allowed_To_Change_To_Payment_Approved()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Aguardando_Pagamento, ESellStatus.Pagamento_Aprovado);
        Assert.IsTrue(allowed);
    }

    [TestMethod]
    public void Status_Waiting_Payment_Should_Not_Be_Allowed_To_Change_To_Delivered()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Aguardando_Pagamento, ESellStatus.Entregue);
        Assert.IsFalse(allowed);
    }

    [TestMethod]
    public void Status_Payment_Approved_Should_Be_Allowed_To_Change_To_Cancelled()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Pagamento_Aprovado, ESellStatus.Cancelada);
        Assert.IsTrue(allowed);
    }

    [TestMethod]
    public void Status_Payment_Approved_Should_Be_Allowed_To_Change_To_Sent_To_Carrier()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Pagamento_Aprovado, ESellStatus.Enviado_Para_Transportadora);
        Assert.IsTrue(allowed);
    }

    [TestMethod]
    public void Status_Payment_Approved_Should_Not_Be_Allowed_To_Change_To_Delivered()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Pagamento_Aprovado, ESellStatus.Entregue);
        Assert.IsFalse(allowed);
    }

    [TestMethod]
    public void Status_Sent_To_Carrier_Should_Be_Allowed_To_Change_To_Delivered()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Enviado_Para_Transportadora, ESellStatus.Entregue);
        Assert.IsTrue(allowed);
    }

    [TestMethod]
    public void Status_Sent_To_Carrier_Should_Not_Be_Allowed_To_Change_To_Cancelled()
    {
        var allowed = ESellStatusValidator.ChangeStatusAllowed(ESellStatus.Enviado_Para_Transportadora, ESellStatus.Cancelada);
        Assert.IsFalse(allowed);
    }
}