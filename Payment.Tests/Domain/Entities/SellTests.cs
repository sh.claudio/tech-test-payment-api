using Payment.Domain.Entities;
using Payment.Domain.Enums;

namespace Payment.Tests.Domain.Entities;

[TestClass]
public class SellTests
{
    [TestMethod]
    public void Should_Allow_Change_Status_From_Waiting_Payment_To_Payment_Approved()
    {
        var sell = new Sell() { Status = ESellStatus.Aguardando_Pagamento };

        sell.ChangeStatus(ESellStatus.Pagamento_Aprovado);

        Assert.AreEqual(sell.Status, ESellStatus.Pagamento_Aprovado);
    }

    [TestMethod]
    public void Should_Not_Allow_Change_Status_From_Waiting_Payment_To_Delivered()
    {
        var sell = new Sell() { Status = ESellStatus.Aguardando_Pagamento };

        sell.ChangeStatus(ESellStatus.Entregue);

        Assert.AreNotEqual(sell.Status, ESellStatus.Entregue);
    }
}