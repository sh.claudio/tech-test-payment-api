using Payment.API.DTO;
using Payment.Domain.Enums;

namespace Payment.Tests.DTO;

[TestClass]
public class UpdateStatusRequestDTOTests
{

    [TestMethod]
    public void Update_Status_Request_Valid_Waiting_Payment()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Aguardando_Pagamento";

        Assert.AreEqual(dto.GetStatus(), ESellStatus.Aguardando_Pagamento);
    }

    [TestMethod]
    public void Update_Status_Request_Valid_Payment_Approved()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Pagamento_Aprovado";

        Assert.AreEqual(dto.GetStatus(), ESellStatus.Pagamento_Aprovado);
    }

    [TestMethod]
    public void Update_Status_Request_Valid_Sent_To_Carrier()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Enviado_Para_Transportadora";

        Assert.AreEqual(dto.GetStatus(), ESellStatus.Enviado_Para_Transportadora);
    }

    [TestMethod]
    public void Update_Status_Request_Valid_Delivered()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Entregue";

        Assert.AreEqual(dto.GetStatus(), ESellStatus.Entregue);
    }

    [TestMethod]
    public void Update_Status_Request_Valid_Cancelled()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Cancelada";

        Assert.AreEqual(dto.GetStatus(), ESellStatus.Cancelada);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Update_Status_Request_Invalid_Empty_String()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Update_Status_Request_Invalid_Status()
    {
        var dto = new UpdateStatusRequestDTO();
        dto.NewStatus = "Status inválido";

        dto.Validate();
    }
}