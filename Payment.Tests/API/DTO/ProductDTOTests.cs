using Payment.API.DTO;

namespace Payment.Tests.DTO;

[TestClass]
public class ProductDTOTests
{

    [TestMethod]
    public void Product_Valid()
    {
        var dto = new ProductDTO();
        dto.Codigo = "7891251215412";
        dto.Descricao = "TV OLED 65 pol";
        dto.Preco = 8500;

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Product_Without_Code()
    {
        var dto = new ProductDTO();
        dto.Codigo = "";
        dto.Descricao = "TV OLED 65 pol";
        dto.Preco = 8500;

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Product_Without_Description()
    {
        var dto = new ProductDTO();
        dto.Codigo = "7891216512";
        dto.Descricao = "";
        dto.Preco = 8500;

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Product_With_Price_Zero()
    {
        var dto = new ProductDTO();
        dto.Codigo = "7891216512";
        dto.Descricao = "TV OLED 65 pol";
        dto.Preco = 0;

        dto.Validate();
    }

}