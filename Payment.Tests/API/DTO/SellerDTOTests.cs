using Payment.API.DTO;

namespace Payment.Tests.DTO;

[TestClass]
public class SellerDTOTests
{

    [TestMethod]
    public void Seller_Valid()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000191";
        dto.EMail = "teste@teste.com";
        dto.Nome = "Joao da Silva";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_Without_CPF()
    {
        var dto = new SellerDTO();
        dto.Cpf = "";
        dto.EMail = "teste@teste.com";
        dto.Nome = "Joao da Silva";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_With_Invalid_CPF()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000000";
        dto.EMail = "teste@teste.com";
        dto.Nome = "Joao da Silva";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_Without_EMail()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000191";
        dto.EMail = "";
        dto.Nome = "Joao da Silva";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_With_Invalid_EMail()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000191";
        dto.EMail = "email";
        dto.Nome = "Joao da Silva";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_Without_Name()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000191";
        dto.EMail = "teste@teste.com";
        dto.Nome = "";
        dto.Telefone = "11999999999";

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Seller_Without_Phone()
    {
        var dto = new SellerDTO();
        dto.Cpf = "00000000191";
        dto.EMail = "teste@teste.com";
        dto.Nome = "João da Silva";
        dto.Telefone = "";

        dto.Validate();
    }
}