using Payment.API.DTO;

namespace Payment.Tests.DTO;

[TestClass]
public class SellRequestDTOTests
{

    [TestMethod]
    public void Sell_Request_Valid()
    {
        var dto = new SellRequestDTO();
        dto.Vendedor = new SellerDTO()
        {
            Cpf = "00000000191",
            EMail = "teste@teste.com",
            Nome = "Jose de Andrade",
            Telefone = "11999999999"
        };
        dto.ItensVendidos!.Add(new SellItemDTO()
        {
            Produto = new ProductDTO()
            {
                Codigo = "78915542325",
                Descricao = "Monitor UltraWide 32 pol",
                Preco = 124
            },
            Quantidade = 2
        });

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Sell_Item_Without_Seller()
    {
        var dto = new SellRequestDTO();
        dto.ItensVendidos!.Add(new SellItemDTO()
        {
            Produto = new ProductDTO()
            {
                Codigo = "78915542325",
                Descricao = "Monitor UltraWide 32 pol",
                Preco = 124
            },
            Quantidade = 2
        });

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Sell_Item_Without_SellItem()
    {
        var dto = new SellRequestDTO();
        dto.Vendedor = new SellerDTO()
        {
            Cpf = "00000000191",
            EMail = "teste@teste.com",
            Nome = "Jose de Andrade",
            Telefone = "11999999999"
        };

        dto.Validate();
    }
}