using Payment.API.DTO;

namespace Payment.Tests.DTO;

[TestClass]
public class SellItemDTOTests
{

    [TestMethod]
    public void Sell_Item_Valid()
    {
        var dto = new SellItemDTO();
        dto.Produto = new ProductDTO()
        {
            Codigo = "78912161651",
            Descricao = "Notebook Dell Inspiron",
            Preco = 120
        };
        dto.Quantidade = 2;

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Sell_Item_Without_Product()
    {
        var dto = new SellItemDTO();
        dto.Quantidade = 2;

        dto.Validate();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Sell_Item_Without_Quantity()
    {
        var dto = new SellItemDTO();
        dto.Produto = new ProductDTO()
        {
            Codigo = "78912161651",
            Descricao = "Notebook Dell Inspiron",
            Preco = 120
        };
        dto.Quantidade = 0;

        dto.Validate();
    }
}